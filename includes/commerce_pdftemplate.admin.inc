<?php

function commerce_pdftemplate_admin_form() {
  $form = array();

  $form['commerce_pdftemplate_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Your PDFtemplate.eu username / eMail address'),
    '#default_value' => variable_get('commerce_pdftemplate_username', '')
  );

  $form['commerce_pdftemplate_invoices'] = array(
    '#type' => 'fieldset',
    '#title' => t('Invoices')
  );

  $form['commerce_pdftemplate_invoices']['commerce_pdftemplate_generate_invoices'] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate invoices via pdftemplate.eu'),
    '#default_value' => variable_get('commerce_pdftemplate_generate_invoices', 0),
  );

  $form['commerce_pdftemplate_invoices']['commerce_pdftemplate_invoice_template_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Template title'),
    '#description' => t('The title of the template file you created and uploaded to www.pdftemplate.eu.'),
    '#default_value' => variable_get('commerce_pdftemplate_invoice_template_title', ''),
  );

  $form['commerce_pdftemplate_invoices']['commerce_pdftemplate_invoice_template_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Template key'),
    '#description' => t('The key of the template file.'),
    '#default_value' => variable_get('commerce_pdftemplate_invoice_template_key', ''),
  );

  $form = system_settings_form($form);

  return $form;
}
